const axios = require("axios");

class DocStore {
  constructor() {
    // axios.get("http://127.0.0.1:8081/documents").then(res => {
    //   this.state = res;
    // });
    // this.state = initialState;
    axios
      .get(`http://127.0.0.1:8081/documents`)
      .then(res => {
        this.state = res.data;
      })
      .catch(error => new Error(error.message || error));
  }

  setState(state) {
    this.state = state;
  }

  getState() {
    return this.state;
  }
}

const docStore = new DocStore();
export default docStore;
