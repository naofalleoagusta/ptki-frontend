import React from "react";

import "../pages/Home.css";
// import MediaQuery from "react-responsive";
// import NavLink from "./NavLink";

const Title = props => (
  <div className="home-title">
    AlticaSearch.
    <small>A PTKI Project.</small>
  </div>
);

export default Title;
