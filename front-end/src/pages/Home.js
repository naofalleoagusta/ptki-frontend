import React, { Component } from "react";
// import logo from './logo.svg';
// import "./App.css";
import { withRouter } from "react-router-dom";
import "./Home.css";
import { Row, Col, Form, InputGroup, Button } from "react-bootstrap";
import Title from "../component/Title";
import axios from "axios";
class Home extends Component {
  state = {
    value: "",
    suggestions: [],
    filters: null,
    and: false,
    or: false,
    method: "",
    query: "",
    dataset: ""
  };
  handleOnClick = () => {
    if (!this.state.and) {
      if (this.state.method === "") {
        axios
          .get(`http://127.0.0.1:8081/search`, {
            params: {
              q: this.state.value
            }
          })
          .then(res => {
            console.log(res);
            this.setState(
              {
                value: this.state.value,
                dataset: res.data
              },
              () => {
                console.log("Masuk");
                this.props.history.push("/search/1", {
                  page: this.props.match.url,
                  value: this.state.value,
                  result: this.state.dataset,
                  method: "",
                  and: false
                });
              }
            );
          })
          .catch(error => new Error(error.message || error));
      } else {
        axios
          .get(`http://127.0.0.1:8081/ranks`, {
            params: {
              q: this.state.value
            }
          })
          .then(res => {
            console.log(res);
            this.setState(
              {
                value: this.state.value,
                dataset: res.data
              },
              () => {
                console.log("Masuk");
                this.props.history.push("/search/1", {
                  page: this.props.match.url,
                  value: this.state.value,
                  result: this.state.dataset,
                  method: this.state.method,
                  and: false
                });
              }
            );
          })
          .catch(error => new Error(error.message || error));
      }
    } else {
      if (this.state.method === "") {
        axios
          .get(`http://127.0.0.1:8081/search`, {
            params: {
              q: this.state.value,
              type: 1
            }
          })
          .then(res => {
            console.log(res);
            this.setState(
              {
                value: this.state.value,
                dataset: res.data
              },
              () => {
                console.log("Masuk");
                this.props.history.push("/search/1", {
                  page: this.props.match.url,
                  value: this.state.value,
                  result: this.state.dataset,
                  method: "",
                  and: false
                });
              }
            );
          })
          .catch(error => new Error(error.message || error));
      } else {
        axios
          .get(`http://127.0.0.1:8081/ranks`, {
            params: {
              q: this.state.value,
              type: 1
            }
          })
          .then(res => {
            console.log(res);
            this.setState(
              {
                value: this.state.value,
                dataset: res.data
              },
              () => {
                console.log("Masuk");
                this.props.history.push("/search/1", {
                  page: this.props.match.url,
                  value: this.state.value,
                  result: this.state.dataset,
                  method: this.state.method,
                  and: false
                });
              }
            );
          })
          .catch(error => new Error(error.message || error));
      }
    }
  };

  onKeyPress = e => {
    if (this.state.value !== "") {
      if (e.which === 13) {
        this.handleOnClick();
      }
    }
  };
  SearchBar = () => {
    return (
      <InputGroup className="mb-3">
        <Form.Control
          className="search-bar m-0"
          placeholder="Type something!"
          onChange={e => {
            this.setState({
              value: e.target.value
            });
          }}
          value={this.state.value}
          onKeyDown={this.onKeyPress}
          type="text"
        />
        <InputGroup.Append>
          <Button
            variant="outline-secondary"
            type="button"
            onClick={this.handleOnClick}
          >
            <i className="fa fa-search"></i>
          </Button>
        </InputGroup.Append>
      </InputGroup>
    );
  };
  render() {
    return (
      <div className="home">
        <Row className="w-100 h-100 m-0 home-content justify-content-center align-items-center">
          <Col xs={12} className="p-0 m-0">
            <Title />
            {!this.state.show ? (
              <>
                <Button
                  variant="dark"
                  onClick={() => {
                    this.setState({
                      show: true,
                      and: true,
                      query: "AND"
                    });
                  }}
                  className="button-query mr-5"
                >
                  AND
                </Button>
                <Button
                  variant="light"
                  onClick={() => {
                    this.setState({
                      show: true,
                      or: true,
                      query: "OR"
                    });
                  }}
                  className="button-query"
                >
                  OR
                </Button>
              </>
            ) : (
              ""
            )}
            {this.state.show && (
              <>
                <this.SearchBar />
                <Form.Check
                  name="method"
                  label="TF-IDF"
                  type="radio"
                  inline
                  className="h5 black font-light"
                  value="tfidf"
                  onChange={e =>
                    this.setState({
                      method: e.target.value
                    })
                  }
                />
                <Form.Check
                  name="method"
                  label="BM25"
                  type="radio"
                  inline
                  className="h5 black font-light"
                  value="bm25"
                  onChange={e =>
                    this.setState({
                      method: e.target.value
                    })
                  }
                />
                <Form.Check
                  name="method"
                  label="Unigram"
                  type="radio"
                  inline
                  className="h5 black font-light"
                  value="unigram"
                  onChange={e =>
                    this.setState({
                      method: e.target.value
                    })
                  }
                />
              </>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

export default withRouter(Home);
