import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import "./Home.css";
import {
  Row,
  Col,
  Form,
  InputGroup,
  Button,
  OverlayTrigger
} from "react-bootstrap";
import Popover from "react-bootstrap/Popover";
import "./SearchResult.css";
import ResultItem from "../component/ResultItem";
//import docStore from "../store/DocStores";
import axios from "axios";
class SearchResult extends Component {
  state = {
    value: "",
    suggestions: [],
    filter: 0,
    title: "",
    content: "",
    dataset: "",
    match: "",
    and: false,
    method: "",
    temp: "",
    booleanQuery: "",
    TP: 0,
    FN: 0,
    precision: 0,
    recall: 0,
    show: false
  };
  componentDidMount = () => {
    window.scrollTo(0, 0);
    console.log(this.props.location.state);
    axios
      .get(`http://127.0.0.1:8081/documents`)
      .then(res => {
        if (this.props.location.state.result.docs) {
          this.setState(
            {
              value: this.props.location.state.value,
              match: this.props.location.state.result,
              dataset: res.data,
              and: this.props.location.state.and,
              method: this.props.location.state.method,
              temp: this.props.location.state.result.docs
            },
            () => {
              console.log(this.state);
              this.setState({
                FN: this.state.match.length
              });
            }
          );
        } else {
          this.setState(
            {
              value: this.props.location.state.value,
              match: this.props.location.state.result,
              dataset: res.data,
              and: this.props.location.state.and,
              method: this.props.location.state.method,
              temp: this.props.location.state.result
            },
            () => {
              if (this.state.method === "tfidf") {
                this.setState({
                  FN: this.state.match.lengthTF
                });
              } else if (this.state.method === "bm25") {
                this.setState({
                  FN: this.state.match.lengthBM
                });
              } else {
                this.setState({
                  FN: this.state.match.lengthUni
                });
              }
            }
          );
        }
        // console.log(dataset);
        // dataset = json;
        // console.log(dataset);
        // dataset = JSON.parse(json);
        // console.log(dataset);
      })
      .catch(error => new Error(error.message || error));

    console.log(this.state);
  };
  showFile = (title, content) => {
    this.setState(
      {
        title: title,
        content: content
      },
      () => {
        document.getElementById("content-show").style.display = "block";
        var str = document.getElementById("right-content").innerHTML;
        var search = this.state.value; // for finding inputs value use ".value"
        var twoWord = this.state.value.split(" ");
        // if (twoWord.length <= 1) {
        //   console.log(twoWord);
        //   var regex = new RegExp(search, "g");
        //   var res = str.replace(regex, "<b>" + search + "</b>");
        //   search = search.toLowerCase();
        //   regex = new RegExp(search, "i");
        //   var result = res.replace(regex, "<b>" + search + "</b>");
        //   document.getElementById("right-content").innerHTML = result;
        // } else {
        //   var regex = new RegExp(twoWord[0], "g");
        //   var res = str.replace(regex, "<b>" + search + "</b>");
        //   search = search.toLowerCase();
        //   regex = new RegExp(twoWord[0].toLowerCase(), "i");
        //   var result = res.replace(regex, "<b>" + search + "</b>");
        //   regex = new RegExp(twoWord[1], "g");
        //   result = res.replace(regex, "<b>" + search + "</b>");
        //   regex = new RegExp(twoWord[1].toLowerCase(), "i");
        //   result = res.replace(regex, "<b>" + search + "</b>");
        //   document.getElementById("right-content").innerHTML = result;
        // }
      }
    );
  };

  addTF = () => {
    const TPNOW = this.state.TP;
    if (!this.state.show) {
      this.setState({
        TP: TPNOW + 1,
        show: true
      });
    } else {
      this.setState({
        TP: TPNOW + 1
      });
    }
  };
  subFN = () => {
    const FNNOW = this.state.FN;
    if (!this.state.show) {
      this.setState({
        FN: FNNOW + 1,
        show: true
      });
    } else {
      this.setState({
        FN: FNNOW + 1
      });
    }
  };
  handleOnClick = () => {
    console.log("masuk");
    window.scrollTo(0, 0);
    document.getElementById("content-show").style.display = "none";
    if (!this.state.and) {
      if (this.state.method === "") {
        axios
          .get(`http://127.0.0.1:8081/search`, {
            params: {
              q: this.state.value
            }
          })
          .then(res => {
            console.log(res);
            this.setState(
              {
                value: this.state.value,
                dataset: res.data
              },
              () => {
                console.log("Masuk");
                this.props.history.push("/search/1", {
                  page: this.props.match.url,
                  value: this.state.value,
                  result: this.state.dataset,
                  method: "",
                  and: false
                });
              }
            );
          })
          .catch(error => new Error(error.message || error));
      } else {
        axios
          .get(`http://127.0.0.1:8081/ranks`, {
            params: {
              q: this.state.value
            }
          })
          .then(res => {
            console.log(res);
            this.setState(
              {
                value: this.state.value,
                dataset: res.data
              },
              () => {
                console.log("Masuk");
                this.props.history.push("/search/1", {
                  page: this.props.match.url,
                  value: this.state.value,
                  result: this.state.dataset,
                  method: this.state.method,
                  and: false
                });
              }
            );
          })
          .catch(error => new Error(error.message || error));
      }
    } else {
      axios
        .get(`http://127.0.0.1:8081/search`, {
          params: {
            q: this.state.value,
            type: 1
          }
        })
        .then(res => {
          console.log(res);
          this.setState(
            {
              value: this.state.value,
              dataset: res.data
            },
            () => {
              console.log("Masuk");
              this.props.history.push("/search/1", {
                page: this.props.match.url,
                value: this.state.value,
                result: this.state.dataset,
                method: "",
                and: false
              });
            }
          );
        })
        .catch(error => new Error(error.message || error));
    }
  };

  onKeyPress = e => {
    if (this.state.value !== "") {
      if (e.which === 13) {
        this.handleOnClick();
      }
    }
  };

  SearchBar = () => {
    return (
      <InputGroup className="">
        <Form.Control
          className="search-bar m-0"
          placeholder="Type something!"
          onChange={e => {
            this.setState({
              value: e.target.value
            });
          }}
          onKeyDown={this.onKeyPress}
          required
          value={this.state.value}
          type="text"
        />
        <InputGroup.Append>
          <Button variant="outline-secondary" onClick={this.handleOnClick}>
            <i className="fa fa-search"></i>
          </Button>
        </InputGroup.Append>
      </InputGroup>
    );
  };

  showMethod = (e, type) => {
    console.log(this.state);
    console.log(type);
    let and = 0;
    if (type === true) {
      and = 1;
    } else {
      and = 0;
    }
    console.log(and);
    axios
      .get(`http://127.0.0.1:8081/ranks`, {
        params: {
          q: this.state.value,
          type: and
        }
      })
      .then(res => {
        console.log(res);
        this.setState(
          {
            match: res.data,
            temp: res.data
          },
          () => {
            console.log(this.state);
          }
        );
      })
      .catch(error => new Error(error.message || error));
  };

  showNone = (e, type) => {
    console.log(this.state);
    let and = 0;
    if (type) {
      and = 1;
    } else {
      and = 0;
    }
    console.log(and);
    this.setState(
      {
        method: e.target.value
      },
      () => {
        axios
          .get(`http://127.0.0.1:8081/search`, {
            params: {
              q: this.state.value,
              type: and
            }
          })
          .then(res => {
            console.log(res);
            this.setState(
              {
                match: res.data,
                temp: res.data
              },
              () => {
                console.log(this.state);
              }
            );
          })
          .catch(error => new Error(error.message || error));
      }
    );
  };
  RenderAllText = props => {
    // console.log(props.dataset);
    console.log(this.props.location.state);
    console.log(props.match);
    let { and, method } = this.state;
    let prop = [];
    if (method === "") {
      for (let i = 0; i < props.match.docs.length; i++) {
        // for(let k=1;k<=props.match.length;k++){
        let idx = props.match.docs[i];
        prop.push(
          <ResultItem
            key={i}
            match
            title={props.dataset[idx].title}
            content={props.dataset[idx].content}
            method={() => {
              this.showFile(
                props.dataset[idx].title,
                props.dataset[idx].content
              );
            }}
          />
        );
        // }
      }
    } else {
      for (let i = 1; i <= props.match.length; i++) {
        // for(let k=1;k<=props.match.length;k++){
        let idx = props.match[i].index;
        prop.push(
          <ResultItem
            key={i}
            match
            addTP={this.addTF}
            subFN={this.subFN}
            title={props.dataset[idx].title}
            content={props.dataset[idx].content}
            method={() => {
              this.showFile(
                props.dataset[idx].title,
                props.dataset[idx].content
              );
            }}
            score={props.match[i].score}
          />
        );
        // }
      }
    }
    return prop;
  };
  render() {
    return (
      <div>
        <Row className="py-2 w-100 sticky-top mx-0 header-result px-5 d-flex align-items-center">
          <Col
            xs={3}
            md={2}
            className="p-0 result-title"
            onClick={() => {
              this.props.history.push("/");
            }}
          >
            <div className="">
              AlticaSearch.
              <small>A PTKI Project.</small>
            </div>
          </Col>
          <Col xs={9} md={6} className="p-0 d-flex align-items-center">
            <this.SearchBar />
          </Col>
          <OverlayTrigger
            rootClose
            trigger="click"
            placement="bottom"
            overlay={
              <Popover className="pop-over-wrapping text-center">
                {this.state.method === "tfidf" ? (
                  <Form.Check
                    name="method"
                    label="TF-IDF"
                    type="radio"
                    inline
                    value="tfidf"
                    defaultChecked
                    onChange={e =>
                      this.setState({
                        method: e.target.value
                      })
                    }
                  />
                ) : (
                  <Form.Check
                    name="method"
                    label="TF-IDF"
                    type="radio"
                    inline
                    value="tfidf"
                    onChange={e =>
                      this.setState({
                        method: e.target.value
                      })
                    }
                  />
                )}
                {this.state.method === "bm25" ? (
                  <Form.Check
                    name="method"
                    label="BM25"
                    type="radio"
                    inline
                    value="bm25"
                    defaultChecked
                    onChange={e =>
                      this.setState({
                        method: e.target.value
                      })
                    }
                  />
                ) : (
                  <Form.Check
                    name="method"
                    label="BM25"
                    type="radio"
                    inline
                    value="bm25"
                    onChange={e =>
                      this.setState({
                        method: e.target.value
                      })
                    }
                  />
                )}
                {this.state.method === "unigram" ? (
                  <Form.Check
                    name="method"
                    label="Unigram"
                    type="radio"
                    inline
                    value="unigram"
                    defaultChecked
                    onChange={e =>
                      this.setState({
                        method: e.target.value
                      })
                    }
                  />
                ) : (
                  <Form.Check
                    name="method"
                    label="Unigram"
                    type="radio"
                    inline
                    value="unigram"
                    onChange={e =>
                      this.setState({
                        method: e.target.value
                      })
                    }
                  />
                )}
                {this.state.method === "" ? (
                  <Form.Check
                    name="method"
                    label="None"
                    type="radio"
                    inline
                    value=""
                    defaultChecked
                    onChange={e =>
                      this.setState({
                        method: e.target.value
                      })
                    }
                  />
                ) : (
                  <Form.Check
                    name="method"
                    label="None"
                    type="radio"
                    inline
                    value=""
                    onChange={e => this.showNone(e, this.state.and)}
                  />
                )}
              </Popover>
            }
          >
            <Button className="ml-2" variant="outline-secondary">
              Change Ranked Method
            </Button>
          </OverlayTrigger>
          <OverlayTrigger
            rootClose
            trigger="click"
            placement="bottom"
            overlay={
              <Popover className="pop-over-wrapping text-center">
                {this.state.and ? (
                  <Form.Check
                    name="boolean"
                    label="AND"
                    type="radio"
                    inline
                    value={true}
                    defaultChecked
                    onChange={e =>
                      this.setState({
                        and: e.target.value
                      })
                    }
                  />
                ) : (
                  <Form.Check
                    name="boolean"
                    label="AND"
                    type="radio"
                    inline
                    value={true}
                    onChange={e =>
                      this.setState(
                        {
                          and: e.target.value
                        },
                        () => {
                          this.state.method === ""
                            ? this.showNone(e, this.state.and)
                            : this.showMethod(e, this.state.and);
                        }
                      )
                    }
                  />
                )}
                {!this.state.and ? (
                  <Form.Check
                    name="boolean"
                    label="OR"
                    type="radio"
                    inline
                    value={false}
                    defaultChecked
                    onChange={e =>
                      this.setState({
                        and: e.target.value
                      })
                    }
                  />
                ) : (
                  <Form.Check
                    name="boolean"
                    label="OR"
                    type="radio"
                    inline
                    value={false}
                    onChange={e =>
                      this.setState(
                        {
                          and: e.target.value
                        },
                        () => {
                          this.state.method === ""
                            ? this.showNone(e, this.state.and)
                            : this.showMethod(e, this.state.and);
                        }
                      )
                    }
                  />
                )}
              </Popover>
            }
          >
            <Button className="ml-2" variant="outline-secondary">
              Change Boolean Query
            </Button>
          </OverlayTrigger>
        </Row>
        <div className="search-result ">
          <Row className="w-100">
            {this.state.method === "" && (
              <Col xs={7}>
                {this.state.match.length > 0 ? (
                  <>
                    <p className="mt-4">
                      {this.state.and
                        ? "Using Boolean Query : AND, "
                        : "Using Boolean Query : OR, "}
                      About {this.state.match.length} result(s) in{" "}
                      {this.state.match.time} milliseconds
                    </p>
                    <this.RenderAllText
                      match={this.state.match}
                      dataset={this.state.dataset}
                    />
                  </>
                ) : (
                  <h2>No doc(s) match!</h2>
                )}
              </Col>
            )}
            {this.state.method === "tfidf" && (
              <Col xs={7}>
                {this.state.match.lengthTF > 0 ? (
                  <>
                    <p className="mt-4">
                      {this.state.and
                        ? "Using Boolean Query : AND, and"
                        : "Using Boolean Query : OR, and"}{" "}
                      method TF-IDF, About {this.state.match.lengthTF} result(s)
                      in {this.state.match.tfidf.time} milliseconds
                    </p>
                    <this.RenderAllText
                      match={this.state.match.tfidf}
                      dataset={this.state.dataset}
                    />
                  </>
                ) : (
                  <h2>No doc(s) match!</h2>
                )}
              </Col>
            )}
            {this.state.method === "bm25" && (
              <Col xs={7}>
                {this.state.match.lengthBM > 0 ? (
                  <>
                    <p className="mt-4">
                      {this.state.and
                        ? "Using Boolean Query : AND, and"
                        : "Using Boolean Query : OR, and"}{" "}
                      method BM25, About {this.state.match.lengthBM} result(s)
                      in {this.state.match.bm25.time} milliseconds
                    </p>
                    <this.RenderAllText
                      match={this.state.match.bm25}
                      dataset={this.state.dataset}
                    />
                  </>
                ) : (
                  <h2>No doc(s) match! </h2>
                )}
              </Col>
            )}
            {this.state.method === "unigram" && (
              <Col xs={7}>
                {this.state.match.lengthUni > 0 ? (
                  <>
                    <p className="mt-4">
                      {this.state.and
                        ? "Using Boolean Query : AND, and"
                        : "Using Boolean Query : OR, and"}{" "}
                      method Unigram, About {this.state.match.lengthUni}{" "}
                      result(s) in {this.state.match.unigram.time} milliseconds
                    </p>
                    <this.RenderAllText
                      match={this.state.match.unigram}
                      dataset={this.state.dataset}
                    />
                  </>
                ) : (
                  <h2>No doc(s) match! </h2>
                )}
              </Col>
            )}
            <Col xs={5}>
              <div className="filter">
                <div className="title-filter">Filter</div>
                <div className="filter-content d-flex align-items-center mt-3">
                  <Form.Control
                    type="text"
                    inline
                    className=" m-0 w-50"
                    value={this.state.filter}
                    onChange={e =>
                      this.setState({
                        filter: e.target.value
                      })
                    }
                  />
                  <Button
                    className="ml-3"
                    onClick={() => {
                      const filter = parseInt(this.state.filter);
                      if (this.props.location.state.method === "") {
                        let arr = this.state.temp;
                        if (arr) {
                          console.log(arr.slice(0, filter));
                          const json = arr.slice(0, filter);
                          const res = {
                            docs: json,
                            time: this.state.match.time,
                            length: json.length
                          };
                          console.log(res);
                          this.setState({
                            match: res
                          });
                        }
                      } else {
                        let arr = this.state.match;
                        if (this.state.method === "tfidf") {
                          console.log(this.state.temp);
                          if (arr) {
                            let tfidf = this.state.temp.tfidf;
                            let json = {};
                            console.log(json);
                            for (let i = 1; i <= filter; i++) {
                              json[i] = tfidf[i];
                            }
                            json["length"] = filter;
                            json["time"] = tfidf.time;
                            console.log(json);

                            const matchJson = {
                              lengthBM: this.state.match.lengthBM,
                              lengthTF: this.state.temp.lengthTF,
                              lengthUni: this.state.match.lengthUni,
                              tfidf: json,
                              bm25: this.state.match.bm25,
                              unigram: this.state.unigram
                            };
                            this.setState({
                              match: matchJson
                            });
                          }
                        } else if (this.state.method === "bm25") {
                          console.log(this.state.temp);
                          if (arr) {
                            let bm25 = this.state.temp.bm25;
                            let json = {};
                            console.log(json);
                            for (let i = 1; i <= filter; i++) {
                              json[i] = bm25[i];
                            }
                            json["length"] = filter;
                            json["time"] = bm25.time;
                            console.log(json);

                            const matchJson = {
                              lengthBM: this.state.match.lengthBM,
                              lengthTF: this.state.temp.lengthTF,
                              lengthUni: this.state.match.lengthUni,
                              tfidf: this.state.match.tfidf,
                              bm25: json,
                              unigram: this.state.unigram
                            };
                            this.setState({
                              match: matchJson
                            });
                          }
                        } else if (this.state.method === "unigram") {
                          console.log(this.state.temp);
                          if (arr) {
                            let unigram = this.state.temp.unigram;
                            let json = {};
                            for (let i = 1; i <= filter; i++) {
                              json[i] = unigram[i];
                            }
                            json["length"] = filter;
                            json["time"] = unigram.time;
                            console.log(json);

                            const matchJson = {
                              lengthBM: this.state.match.lengthBM,
                              lengthTF: this.state.temp.lengthTF,
                              lengthUni: this.state.match.lengthUni,
                              tfidf: this.state.match.tfidf,
                              bm25: this.state.unigram,
                              unigram: json
                            };
                            this.setState({
                              match: matchJson
                            });
                          }
                        }
                      }
                    }}
                  >
                    Set Filter!
                  </Button>
                </div>
                {this.state.show && (
                  <div className="title-filter mt-4">
                    Precision :{" "}
                    {((this.state.TP * 1.0) / (this.state.TP + this.state.FN)) *
                      100}{" "}
                    %
                  </div>
                )}
                {/* <div className="title-filter">Precision : {(this.state.TP*1.0/(this.state.TP+this.state.FP)*100)} %</div> */}
              </div>

              <div id="content-show" className="content">
                <div xs={12} className="title-content">
                  {this.state.title}
                </div>
                <div id="right-content" className="right-content">
                  {this.state.content}
                </div>
              </div>
            </Col>
          </Row>
          {/* <h1>TEST</h1>
          asdasndhaldnkasdaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa */}
        </div>
      </div>
    );
  }
}
export default withRouter(SearchResult);
